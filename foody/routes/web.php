<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('map','MapController@show');
// Route::get('map/{lat}/{log}','MapController@map');
Route::get('login','admin\LoginController@getLogin')->name('login')->middleware('CheckLogout');
Route::post('login','admin\LoginController@postLogin');
Route::group(['prefix' => 'admin','namespace'=>'admin','middleware'=>'CheckLogin'], function () {
    Route::get('/','HomeController@index')->name('home');
    Route::get('logout','LoginController@logOut')->name('logout');
    Route::get('/user/detail/{id}','UserController@userDetail')->name('get.user');
    Route::post('/user/detail/{id}','UserController@editUser');
    Route::group(['prefix' => 'restaurant'], function () {
        Route::get('/','RestaurantController@list')->name('list.res');
        Route::get('/add','RestaurantController@addRestaurant')->name('add.res');
        Route::post('/add','RestaurantController@postAddRestaurant');
        Route::get('/edit/{id}','RestaurantController@editRestaurant')->name('edit.res');
        Route::post('/edit/{id}','RestaurantController@postEditRestaurant');
        Route::get('/del/{id}','RestaurantController@delRestaurant')->name('del.res');
    });
    Route::group(['prefix' => 'product'], function () {
        Route::get('/list','ProductController@listProduct')->name('list.product');
        Route::get('/add','ProductController@addProduct')->name('add.product');
        Route::post('/add','ProductController@postAddProduct');
        Route::get('list/edit/{id}','ProductController@editProduct')->name('edit.product');
        Route::post('list/edit/{id}','ProductController@postEditProduct');
        Route::get('list/del/{id}','ProductController@delProduct')->name('del.product');
    });
});

