<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            ['id'=>1,'email'=>'admin@gmail.com','password'=>bcrypt('123456'),'fist_name'=>'nguyen','last_name'=>'Văn Công','phone'=>'0123456789','address'=>'Thường tín','level'=>1],
            ['id'=>2,'email'=>'user1@gmail.com','password'=>bcrypt('123456'),'fist_name'=>'thế vũ','last_name'=>'Văn Công','phone'=>'012346789','address'=>'Bắc giang','level'=>2],
            ['id'=>3,'email'=>'user2@gmail.com','password'=>bcrypt('123456'),'fist_name'=>'thế','last_name'=>'Công','phone'=>'012456789','address'=>'Huế','level'=>2],
            ['id'=>4,'email'=>'user3@gmail.com','password'=>bcrypt('123456'),'fist_name'=>'Văn','last_name'=>'Công','phone'=>'012345689','address'=>'Nghệ An','level'=>2],
        ]);
    }
}
