<?php

use Illuminate\Database\Seeder;

class RestaurantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restaurants')->delete();
        DB::table('restaurants')->insert([
            ['id'=>1,'res_name'=>'Nhà Hàng Hồng Hạnh','city'=>'Hà Nội','address'=>'188 Quan Nhân, Nhân Chính, Thanh Xuân','phone'=>'0123456789','long'=>'105.813420','lat'=>'21.008760','image'=>'anh1.jpg','time_on'=>'7:00','time_off'=>'22:30','user_id'=>'2'],
            ['id'=>2,'res_name'=>'Nhà hàng Long Phụng','city'=>'Hà Nội','address'=>'29T2 Tòa Nhà, Hoàng Đạo Thúy,Trung Hòa, Cau Giay','phone'=>'0123456789','long'=>'105.801430','lat'=>'21.008430','image'=>'anh1.jpg','time_on'=>'7:00','time_off'=>'22:30','user_id'=>'2'],
            ['id'=>3,'res_name'=>'Nhà Hàng Hải Yến','city'=>'Hà Nội','address'=>'166 Đường Khương Đình, Hạ Đình, Thanh Xuân','phone'=>'0123456789','long'=>'105.814070','lat'=>'20.996530','image'=>'anh1.jpg','time_on'=>'7:00','time_off'=>'22:30','user_id'=>'3'],
            ['id'=>4,'res_name'=>'Nhà Hàng','city'=>'Hà Nội','address'=>'166 Đường Khương Đình,Thanh Xuân','phone'=>'0123456789','long'=>'105.8293101','lat'=>'20.990020','image'=>'anh1.jpg','time_on'=>'7:00','time_off'=>'22:30','user_id'=>'4',]
        ]);

    }
}
