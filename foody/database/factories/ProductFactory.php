<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Model\Product::class, function (Faker $faker) {
    return [
        'product_name'=>$faker->lastName,
        'price'=>$faker->numberBetween($min = 50000, $max = 500000),
        'featured'=>$faker->numberBetween($min =0, $max = 1),
        'product_img'=>'anh.jpg',
        'category_id'=>App\Model\Category::all()->random()->id,
        'restaurant_id'=>App\Model\Restaurant::all()->random()->id,
    ];
});
