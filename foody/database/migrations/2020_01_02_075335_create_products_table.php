<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_name',50);
            $table->decimal('price', 18, 0)->default(0);
            $table->tinyInteger('featured')->unsigned();
            $table->integer('sale_off')->default(0);
            $table->string('product_img');
            $table->integer('category_id')->unsigned();
            $table->integer('restaurant_id')->unsigned();

            $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade');
            $table->foreign('restaurant_id')->references('id')->on('restaurants')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
