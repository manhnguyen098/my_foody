@extends('backend.master.master')
@section('title','Danh sách nhà hàng ')
@section('res')
    active
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Restaurant</h4>
              <p class="card-category"></p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>
                      Name
                    </th>
                    <th>
                      Address
                    </th>
                    <th>
                      City
                    </th>
                    <th>
                      Long
                    </th>
                    <th>
                      Lat
                    </th>
                    <th>
                      Phone
                    </th>
                    <th>
                      Time on
                    </th>
                    <th>
                      Time off
                    </th>
                    <th>
                      Image
                    </th>
                    <th style="text-align: center">
                      Action
                    </th>
                  </thead>
                  <tbody>
                    @foreach ($res as $row)
                    <tr>
                      <td>
                        {{ $row->res_name }}
                      </td>
                      <td>
                        {{ $row->address }}
                      </td>
                      <td>
                        {{ $row->city }}
                      </td>
                      <td>
                        {{ $row->long }}
                      </td>
                      <td>
                        {{ $row->lat }}
                      </td>
                      <td>
                        {{ $row->phone }}
                      </td>
                      <td>
                        {{ $row->time_on }}
                      </td>
                      <td>
                        {{ $row->time_off }}
                      </td>
                      <td class="text-primary">
                        <div class="img-xx"><img src="{{ asset('backend/images/anh1.jpg') }}" alt="">
                        </div>
                      </td>
                      <td>
                        <a href="restaurant/edit/{{ $row->id }}" class="btn btn-primary pull-right">Edit</a>
                        <a href="restaurant/del/{{ $row->id }}" class="btn btn-primary pull-right">Del</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
@endsection