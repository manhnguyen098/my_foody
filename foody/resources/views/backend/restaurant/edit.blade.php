@extends('backend.master.master')
@section('title','Sửa Nhà Hàng')
@section('res')
    active
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Sửa Thông Tin Nhà Hàng</h4>
             
            </div>
            <div class="card-body">
              <form method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Tên</label>
                      <input name="name" type="text" class="form-control" value="{{ $res->res_name }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Thành Phố</label>
                      <input name="city" type="text" class="form-control" value="{{ $res->city }}">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Địa chỉ cụ thể</label>
                      <input name="address" type="text" class="form-control" value="{{ $res->address }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Số điện thoại</label>
                      <input name="phone" type="text" class="form-control" value="{{ $res->phone }}">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Lat</label>
                      <input name="lat" type="text" class="form-control" value="{{ $res->lat }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Long</label>
                      <input name="long" type="text" class="form-control" value="{{ $res->long }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Giờ mở cửa</label>
                      <input name="time_on" type="text" class="form-control" value="{{ $res->time_on }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Giờ đóng cửa</label>
                      <input name="time_off" type="text" class="form-control" value="{{ $res->time_off }}">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <input id="image" name="image" type="file" class="form-control">
                  </div>
                </div>
                <button type="submit" class="btn btn-primary pull-right">Sửa</button>
                <div class="clearfix"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection