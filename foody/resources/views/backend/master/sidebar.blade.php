<div class="logo">
    <a href="http://www.creative-tim.com" class="simple-text logo-normal">
      Quản lý
    </a>
  </div>
<div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item  @yield('home')">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
          <p>Trang chủ</p>
        </a>
      </li>
      <li class="nav-item  @yield('user')">
        <a class="nav-link" href="{{ route('get.user',['id'=>Auth::user()->id]) }}">
          <i class="material-icons">person</i>
          <p>Tài Khoản</p>
        </a>
      </li>
      
      <li class="nav-item @yield('res')">
        <a class="nav-link" href="{{ route('list.res') }}">
          <i class="material-icons">content_paste</i>
          <p>Danh sách nhà hàng</p>
        </a>
      </li>
      <li class="nav-item @yield('addres')">
        <a class="nav-link" href="{{ route('add.res') }}">
          <i class="fa fa-plus"></i>
          <p>Thêm nhà hàng</p>
        </a>
      </li>
      <li class="nav-item @yield('product')">
        <a class="nav-link" href="{{ route('list.product') }}">
          <i class="material-icons">content_paste</i>
          <p>Danh sách sản phẩm</p>
        </a>
      </li>
      <li class="nav-item @yield('addproduct')">
        <a class="nav-link" href="{{ route('add.product') }}">
          <i class="fa fa-plus"></i>
          <p>Thêm sản phẩm</p>
        </a>
      </li>
    </ul>
  </div>