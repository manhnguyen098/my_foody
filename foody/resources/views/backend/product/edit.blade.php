@extends('backend.master.master')
@section('title','Sửa Sản phẩm')
@section('addproduct')
    active
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Sửa sản phẩm</h4>
             
            </div>
            <div class="card-body">
              <form method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Tên sản phẩm</label>
                      <input name="name" type="text" class="form-control" value="{{ $prd->product_name }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Giá</label>
                      <input name="price" type="text" class="form-control" value="{{ $prd->price }}">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Khuyến mãi</label>
                      <input name="sale_off" type="text" class="form-control" value="{{ $prd->sale_off }}">
                    </div>
                  </div>
                  
                </div>
                <div class="row">
                  <div class="col-md-6">
                      <label>Sản phẩm nổi bật</label>
                          <select  name="featured" class="form-control">
                              <option @if ($prd->featured == 0)
                                selected
                              @endif value="0">Không</option>
                              <option @if ($prd->featured == 1)
                                selected
                              @endif value="1">Có</option>
                          </select>
                  </div>
              </div> 
              <div class="row">
                <div class="col-md-6">                    
                      <label>Danh mục</label>
                            <select class="form-control" name="category">
                              {{ GetCategory($category,0,"",$prd->category_id)}}
                            </select>                 
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">                    
                        <label>Nhà hàng</label>
                          <select class="form-control" name="restaurant">                              
                              {{ getRes($res,$prd->restaurant_id) }}                             
                          </select>
                </div>
              </div>
                </div>                               
                  <div class="col-md-3">
                    <input name="image" id="image" type="file" class="form-control">                
                </div>
                <button type="submit" class="btn btn-primary pull-right">Sửa sản phẩm</button>
                <div class="clearfix"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection