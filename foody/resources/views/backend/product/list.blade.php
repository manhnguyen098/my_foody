@extends('backend.master.master')
@section('title','Danh sách sản phẩm')
@section('product')
    active
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Danh sách sản phẩm</h4>
              <p class="card-category"></p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>
                      Tên
                    </th>
                    <th>
                      Giá
                    </th>
                    <th>
                      Nổi bật
                    </th>
                    <th>
                      Khuyến mại
                    </th>
                    <th>
                      Danh mục
                    </th>
                    <th>
                      Nhà hàng
                    </th>
                    <th>
                      Ảnh
                    </th>
                    <th style="text-align: center">
                      Action
                    </th>
                  </thead>
                  <tbody>
                    @foreach ($prd as $row)
                        
                   
                    <tr>
                      <td>
                        {{ $row->product_name }}
                      </td>
                      <td>
                        {{number_format($row->price,0,",",",")}} VND
                      </td>
                      <td>
                        @if ($row->featured == 1)
                        <p>&#10004;
                        @endif
                      </td>
                      <td>
                        {{ $row->sale_off }}
                      </td>
                      <td class="text-primary">
                        
                      </td>
                      <td class="text-primary">
                        {{ $row->res_name }}
                      </td>
                      <td class="text-primary">
                        <div class="img-xx"><img src="{{ asset('backend/images/layer1.jpg') }}" alt="">
                        </div>
                      </td>
                      <td>
                        <a href="{{ route('edit.product',['id'=> $row->id]) }}" class="btn btn-primary pull-right">Sửa</a>
                        <a href="{{ route('del.product',['id'=> $row->id]) }}" class="btn btn-primary pull-right">Xóa</a>
                      </td>
                    </tr>
                    @endforeach

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
@endsection