<?php

namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\Restaurant;
use DB;
use App\Model\Category;

class ProductController extends Controller
{
    public function listProduct(){
        $data['prd'] = DB::table('restaurants')->join('products','products.restaurant_id','=','restaurants.id')
        ->where('user_id','=',Auth::user()->id)
        ->get();
        // dd($data);
        return view('backend.product.list',$data);
    }
    public function addProduct(){
        $data['res'] = Restaurant::where('user_id','=',Auth::user()->id)->get();
        $data['category'] = Category::all()->toArray();
        return view('backend.product.add',$data);
    }
    public function postAddProduct(Request $r){
        $prd = new Product;
        $prd->product_name = $r->name;
        $prd->price = $r->price;
        $prd->sale_off = $r->sale_off;
        $prd->featured = $r->featured;
        $prd->category_id = $r->category;
        $prd->restaurant_id = $r->restaurant;
        if($r->hasFile('image')){
            $image = $r->file('image');
            $filename = $image->getClientOriginalName();
            $image->move('backend/image',$filename);
            $prd->product_img = $filename;
        }
        $prd->save();
        return redirect('admin/product/list');
    }
    public function editProduct($id){
        $data['prd'] = Product::find($id);
        $data['res'] = Restaurant::where('user_id','=',Auth::user()->id)->get()->toArray();
        // dd($data['res']);
        $data['category'] = Category::all()->toArray();
        return view('backend.product.edit',$data);
    }
    public function postEditProduct(Request $r,$id){
        $prd = Product::find($id);
        $prd->product_name = $r->name;
        $prd->price = $r->price;
        $prd->sale_off = $r->sale_off;
        $prd->featured = $r->featured;
        $prd->category_id = $r->category;
        $prd->restaurant_id = $r->restaurant;
        if($r->hasFile('image')){
            $image = $r->file('image');
            $filename = $image->getClientOriginalName();
            $image->move('backend/image',$filename);
            $prd->product_img = $filename;
        }
        $prd->save();
        return redirect('admin/product/list');

    }
    public function delProduct($id){
        Product::destroy($id);
        return redirect()->back();
    }
}
