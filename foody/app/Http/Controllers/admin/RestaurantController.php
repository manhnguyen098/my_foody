<?php

namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Restaurant;

class RestaurantController extends Controller
{
    public function list(){
        $data['res'] = Restaurant::where('user_id',Auth::user()->id)->get();
        return view('backend.restaurant.list',$data);
    }
    public function addRestaurant(){
        return view('backend.restaurant.add');
    }
    public function postAddRestaurant(Request $r){
        $res = new Restaurant;
        $res->name = $r->name;
        $res->city = $r->city;
        $res->address = $r->address;
        $res->phone = $r->phone;
        $res->long = $r->long;
        $res->lat = $r->lat;
        $res->time_on = $r->time_on;
        $res->time_off = $r->time_off;
        $res->user_id = Auth::user()->id;
        if($r->hasFile('image')){
            $image = $r->file('image');
            $filename = $image->getClientOriginalName();
            $image->move('backend/image',$filename);
            $res->image = $filename;
        }
        $res->save();
        return redirect('admin/restaurant');
    }
    public function editRestaurant($id){
        $data['res'] = Restaurant::find($id);
        return view('backend.restaurant.edit',$data);
    }
    public function postEditRestaurant(Request $r,$id){
        $res = Restaurant::find($id);
        $res->name = $r->name;
        $res->city = $r->city;
        $res->address = $r->address;
        $res->phone = $r->phone;
        $res->long = $r->long;
        $res->lat = $r->lat;
        $res->time_on = $r->time_on;
        $res->time_off = $r->time_off;
        if($r->hasFile('image')){
            $image = $r->file('image');
            $filename = $image->getClientOriginalName();
            $image->move('backend/image',$filename);
            $res->image = $filename;
        }
        $res->save();
        return redirect('admin/restaurant');
    }
    public function delRestaurant($id){
        Restaurant::destroy($id);
        return redirect('admin/restaurant');
    }

}
