<?php

namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;

class LoginController extends Controller
{
    public function getLogin(){
        return view('backend.login.login');
    }
    public function postLogin(Request $request){
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return redirect('admin');
        }else{
            return redirect('login');
        }
    }
    public function logOut(){
        Auth::logout();
        return redirect('login');
    }
}
