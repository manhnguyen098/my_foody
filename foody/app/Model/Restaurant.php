<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $table = "restaurants";
    protected $timestamp = false;
    public function products()
    {
        return $this->hasMany('App\Model\Product','restaurant_id');
    }
}
