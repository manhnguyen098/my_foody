<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";
    public function restaurant()
    {
        return $this->belongsTo('App\Model\Restaurant');
    }
    public function category()
    {
        return $this->belongsTo('App\Model\Category');
    }
}
